'use strict';
/*eslint camelcase:0*/
import shared from '../../config/environment/shared';

export default function(sequelize, DataTypes) {
  return sequelize.define('Documents', {
    _id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    file_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    file_extension: {
      type: DataTypes.STRING,
      allowNull: false
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false
    },
    original_document_type: {
      type: DataTypes.ENUM(shared.documentTypes),
      allowNull: false
    },
    new_document_type: {
      type: DataTypes.ENUM(shared.documentTypes),
      allowNull: true
    }
  });
}
