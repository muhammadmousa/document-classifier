'use strict';
/*eslint camelcase:0*/

import sqldb from '../sqldb';
import config from './environment/';

export default function seedDatabaseIfNeeded() {
  if(config.seedDB) {
    let Documents = sqldb.Documents;

    return Documents.sync({force: true})
      .then(() => {
        let document = Documents.bulkCreate([{
          url: 'assets/files/one.pdf',
          file_name: 'one.pdf',
          file_extension: 'pdf',
          original_document_type: 'Other',
          new_document_type: null
        }, {
          url: 'assets/files/two.png',
          file_name: 'two.png',
          file_extension: 'png',
          original_document_type: 'Other',
          new_document_type: null
        }, {
          url: 'assets/files/three.jpg',
          file_name: 'three.jpg',
          file_extension: 'jpg',
          original_document_type: 'Other',
          new_document_type: null
        }, {
          url: 'assets/files/four.gif',
          file_name: 'four.gif',
          file_extension: 'gif',
          original_document_type: 'Other',
          new_document_type: null
        }, {
          url: 'assets/files/five.pdf',
          file_name: 'five.pdf',
          file_extension: 'pdf',
          original_document_type: 'Other',
          new_document_type: null
        }, {
          url: 'assets/files/six.pdf',
          file_name: 'six.pdf',
          file_extension: 'pdf',
          original_document_type: 'Other',
          new_document_type: null
        }
        ]);
        return document;
      })
      .then(() => console.log('finished populating things'))
      .catch(err => console.log('error populating things', err));
  }
}
