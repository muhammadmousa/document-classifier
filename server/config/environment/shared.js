'use strict';

exports = module.exports = {
  // List of user roles
  userRoles: ['guest', 'user', 'admin'],
  documentTypes: [
    'Undefined',
    'Other',
    'Photo',
    'Drawing',
    'Report',
    'Model',
    'Internet',
    'Letter',
    'Engineering drawing',
    'Details',
    'Foundation plan',
    'Computations',
    'Planning application',
    'Depth soundings',
    'Floors',
    'Situation',
    'Renovation',
    'Technical'
  ]
};
