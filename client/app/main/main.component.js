import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './main.routes';
import angularpdf from 'angular-pdf/dist/angular-pdf.min';
import pdf from 'pdfjs-dist/build/pdf';

export class MainController {

  /*@ngInject*/
  constructor($http, $scope, $state) {
    'ngInject';
    this.$http = $http;
    this.$scope = $scope;

    this.$scope.$watchCollection('mySelectedItems', function() {
      if($scope.mySelectedItems && $scope.mySelectedItems.length > 0) {
        let documentId = $scope.mySelectedItems[0]._id;
        $state.go('onedocument', {documentId: documentId});
      }
    });

  }

  $onInit() {
    console.log('All documents', this.documents);
  }
}

export default angular.module('documentClassifierSayanee3NoauthApp.main', [uiRouter])
  .config(routing)
  .component('main', {
    template: require('./main.html'),
    controller: MainController,
    bindings: {
      documents: '<'
    }
  })
  .name;
