'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('main', {
    url: '/',
    template: '<main documents="$resolve.documents.data"></main>',
    resolve: {
      documents: function documents($http) {
        return $http.get('/api/documents/');
      }
    }
  });
}
