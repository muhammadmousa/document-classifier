'use strict';

export function routeConfig($urlRouterProvider, $locationProvider, $qProvider) {
  'ngInject';

  $urlRouterProvider.otherwise('/');

  $locationProvider.html5Mode(true);

  $qProvider.errorOnUnhandledRejections(false);
}
