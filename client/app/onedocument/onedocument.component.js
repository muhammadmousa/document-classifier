import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './onedocument.routes';
import angularpdf from 'angular-pdf/dist/angular-pdf.min';
import pdf from 'pdfjs-dist/build/pdf';

export class OneDocumentController {

  /*@ngInject*/
  constructor($http, $scope, $document, $location, $window, $state, appConfig, $cacheFactory) {
    'ngInject';

    let vm = this;
    vm.$scope = $scope;
    vm.$http = $http;
    vm.$document = $document;
    vm.$location = $location;
    vm.$window = $window;
    vm.$state = $state;
    vm.$cacheFactory = $cacheFactory;

    vm.loading = 'Loading file...';
    vm.scroll = 0;
    vm.documentTypes = appConfig.documentTypes;

    vm.set_new_document_type = function() {
      vm.$http.patch('/api/documents/' + vm.document._id,
        [
          {
            op: 'replace',
            path: '/new_document_type',
            value: vm.document.new_document_type
          }
        ]
      )
        .then(() => {
          // Invalidate the cache
          vm.$cacheFactory.get('$http').remove('/api/documents/' + vm.document._id);
          vm.$cacheFactory.get('$http').remove('/api/documents');
        });
    };

    vm.getNext = function() {
      let idx = vm.idlist.indexOf(vm.document._id);

    }

    vm.getPrevious = function() {

    }

    // ----- START PDFJS FUNCTIONS -----
    vm.getNavStyle = function(scroll) {
      if(scroll > 100) return 'pdf-controls fixed';
      else return 'pdf-controls';
    };

    vm.onError = function(error) {
      console.log(error);
    };

    vm.onLoad = function() {
      $scope.loading = '';
    };

    vm.onProgress = function(progressData) {
      //console.log(progressData);
    };

    vm.onPassword = function(updatePasswordFn, passwordResponse) {
      if(passwordResponse === PDFJS.PasswordResponses.NEED_PASSWORD) {
        updatePasswordFn($scope.pdfPassword);
      } else if(passwordResponse === PDFJS.PasswordResponses.INCORRECT_PASSWORD) {
        console.log('Incorrect password');
      }
    };
    // ----- END PDFJS FUNCTIONS -----
  }

  $onInit() {
    this.idlist = this.idlist.map(function(key) {
      return key._id;
    });

    this.iddict = {};

    this.idlist.forEach((e, i) => {
      this.iddict[e] = {};
      if(i === 0) {
        this.iddict[e].prev = null;
        this.iddict[e].next = this.idlist[i + 1];
      } else if(i === this.idlist.length - 1) {
        this.iddict[e].prev = this.idlist[i - 1];
        this.iddict[e].next = null;
      } else {
        this.iddict[e].prev = this.idlist[i - 1];
        this.iddict[e].next = this.idlist[i + 1];
      }
    });

    this.$scope.$emit('iddict', this.iddict);
  }
}

export default angular.module('documentClassifierSayanee3NoauthApp.onedocument', [uiRouter])
  .config(routing)
  .component('onedocument', {
    template: require('./onedocument.html'),
    controller: OneDocumentController,
    controllerAs: 'vm',
    bindings: {
      document: '<',
      idlist: '<'
    }
  })
  .name;
