'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('onedocument', {
    url: '/:documentId',
    template: '<onedocument document="$resolve.document.data" idlist="$resolve.idlist.data"></onedocument>',
    resolve: {
      document: function document($http, $stateParams) {
        return $http.get('/api/documents/' + $stateParams.documentId);
      },
      idlist: function idlist($http) {
        return $http.get('/api/documents/idlist');
      }
    }
  });
}
