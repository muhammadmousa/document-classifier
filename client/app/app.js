'use strict';

import angular from 'angular';
// import ngAnimate from 'angular-animate';
import ngCookies from 'angular-cookies';
import ngResource from 'angular-resource';
import ngSanitize from 'angular-sanitize';
import ngMaterial from 'angular-material';

import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';

import 'tr-ng-grid';

import {
  routeConfig
} from './app.config';

import main from './main/main.component';
import onedocument from './onedocument/onedocument.component';
import constants from './app.constants';
import util from '../components/util/util.module';
import header from '../components/header/header.component';

import './app.scss';

angular.module('documentClassifierApp', [ngCookies, ngResource, ngSanitize, ngMaterial, uiRouter,
  uiBootstrap, main, onedocument, header, constants, util, 'pdf', 'trNgGrid'
])
  .config(routeConfig)
  .config(function($mdThemingProvider) {
    'ngInject';
    $mdThemingProvider.theme('dark')
      .primaryPalette('light-blue', {
        default: '800',
        'hue-1': '600',
        'hue-2': '400',
        'hue-3': 'A100'
      })
      .dark();
    $mdThemingProvider.theme('light')
      .primaryPalette('light-blue', {
        default: '800',
        'hue-1': '600',
        'hue-2': '400',
        'hue-3': 'A100'
      });
    $mdThemingProvider.theme('default')
      .primaryPalette('light-blue', {
        default: '800',
        'hue-1': '600',
        'hue-2': '400',
        'hue-3': 'A100'
      });
  })
  .run(function($document, $state, $rootScope) {
    'ngInject';

    let iddict = {};

    $rootScope.$on('iddict', function(evt, data) {
      iddict = data;
    });

    $document.on('keydown', function(e) {
      let documentId = $state.params.documentId;
      if(e.key === 'z' && iddict[documentId].prev) {
        $state.go('onedocument', {documentId: iddict[documentId].prev});
      } else if(e.key === 'x' && iddict[documentId].next) {
        $state.go('onedocument', {documentId: iddict[documentId].next});
      }
    });
  });

angular.element(document)
  .ready(() => {
    angular.bootstrap(document, ['documentClassifierApp'], {
      strictDi: true
    });
  });
