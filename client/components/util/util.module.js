'use strict';

import angular from 'angular';
import {
  UtilService
} from './util.service';

export default angular.module('documentClassifierSayanee3NoauthApp.util', [])
  .factory('Util', UtilService)
  .name;
